<?php

$urls = file('urls');
$url = trim($urls[array_rand($urls)]);

header("Location: $url", false, 307);

?>
<!DOCTYPE html>
<html xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>307 Temporary Redirect</title>
</head>
<body>
<h1>Moved Permanently</h1>
<p>The document has moved <a href="<?php print $url ?>">here</a>.</p>
</body>
</html>
